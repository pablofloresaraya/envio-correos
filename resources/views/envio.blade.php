@extends('layouts.plantilla')

@section('title','envio ofertas')

@section('contents')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6">            
                <h1>Zona de Ofertas</h1>
            </div>            
        </div>
        <br>
        <div class="row justify-content-center">
            <div class="col-6">
                <form action="{{route('ofertas.store')}}" method="POST">
                    @csrf <!--token-->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-4 col-form-label control-label input-label text-right">
                                Nombre:                                             
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="name" class="w-100" value="{{old('name')}}">
                            </div>
                        </div>
                        @error('name')                        
                            <div class="row">                                          
                                <div class="col-sm-12 text-danger text-right">*{{$message}}</div>
                            </div>                        
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-4 col-form-label control-label input-label text-right">
                                Nombre amigo:                                             
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="namedest" class="w-100" value="{{old('namedest')}}">
                            </div>
                        </div>
                        @error('namedest')
                        <div class="row">                                          
                            <div class="col-sm-12 text-danger text-right">*{{$message}}</div>
                        </div>                                               
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-4 col-form-label control-label input-label text-right">
                                Correo amigo:                                             
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="correo" class="w-100" value="{{old('correo')}}">
                            </div>
                        </div>
                        @error('correo')                    
                            <div class="row">                                          
                                <div class="col-sm-12 text-danger text-right">*{{$message}}</div>
                            </div>                    
                        @enderror
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-2">    
                            <button class="btn btn-primary btn-block" type="submit">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
