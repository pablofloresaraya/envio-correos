@extends('layouts.plantilla')

@section('title','envio ofertas')

@section('contents')
    <h1>welcome create</h1>
    <br>
    <form action="{{route('cursos.store')}}" method="POST">
        @csrf <!--token-->
        <label>
            Nombre:
            <br>                
            <input type="text" name="name" value="{{old('name')}}">
        </label>

        @error('name')
            <br>
            <small>*{{$message}}</small>
            <br>
        @enderror

        <br>
        <label>
            Nombre amigo:
            <br>                
            <input type="text" name="name2" value="{{old('name2')}}">
        </label>
        
        @error('name2')
        <br>
        <small>*{{$message}}</small>
        <br>
        @enderror

        <br>
        <label>
            Correo:
            <br>               
            <input type="text" name="correo" value="{{old('correo')}}">
        </label>

        @error('correo')
        <br>
        <small>*{{$message}}</small>
        <br>
        @enderror
        <br>
        <button type="submit">Enviar</button>
    </form>
@endsection
