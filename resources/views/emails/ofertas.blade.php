<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body >
        <h1>Zona Ofertas</h1>        
        <p>Estimado(a) {{$contacto['namedest']}}:</p> 
        <p>Junto con saludar, queremos informar de nuestros nuevos productos en ofertas.</p>
        <br>
        <p>Atte. <strong>{{$contacto['name']}}</strong></p> 
    </body>
</html>
