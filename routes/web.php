<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ofertasController;

use App\Mail\ofertasMailable;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('envio');
})->name('index.envio');

Route::post('ofertas', [ofertasController::class, 'store'])->name('ofertas.store');

Route::get('enviado', function () {
    return view('email_enviado');
})->name('correo.enviado');

/*Route::get('correo', function () {
    
    $correo = new ofertasMailable;
    Mail::to('pablofloresaraya@gmail.com')->send($correo);
    
    return 'mensaje enviado';

})->name('correo.ofertas');*/
